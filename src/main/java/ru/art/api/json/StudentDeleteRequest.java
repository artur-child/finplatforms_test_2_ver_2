package ru.art.api.json;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

@Data
@Accessors(chain = true)
public class StudentDeleteRequest {
    @NotNull
    private Long id;
}
