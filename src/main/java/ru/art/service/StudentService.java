package ru.art.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.art.repository.Student;
import ru.art.repository.StudentRepository;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class StudentService {
    private final Converter converter;
    private final StudentRepository studentRepository;

    public StudentDTO addStudent(StudentDTO studentDTO) {
        Student student = converter.convert(studentDTO, Student.class);
        return converter.convert(studentRepository.save(student), StudentDTO.class);
    }

    public boolean deleteStudent(StudentDTO studentDTO) {
        Student student = converter.convert(studentDTO, Student.class);
        if (!studentRepository.existsById(student.getId())) {
            return false;
        } else {
            studentRepository.delete(student);
            return true;
        }
    }

    public List<StudentDTO> listOfStudents() {
        return studentRepository.findAll().stream()
                .map(m -> converter.convert(m, StudentDTO.class))
                .collect(Collectors.toList());
    }
}
