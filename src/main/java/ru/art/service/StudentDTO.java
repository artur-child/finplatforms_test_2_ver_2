package ru.art.service;

import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDate;

@Data
@Accessors(chain = true)
public class StudentDTO {
    private Long id;
    private String firstName;
    private String secondName;
    private String thirdName;
    private LocalDate birthDate;
    private String group;
}
