### Тестовое задание для АО «Финансовые платформы»

#### Задача 2 (ver #2)

Имеется embedded реляционная база данных (h2, hsqldb, sqlite и т.д. - любая sql embedded БД). Завести в БД таблицу
данных о студентах, которая будет содержать: имя, фамилия, отчество, дата рождения, группа, уникальный номер.
Реализовать консольный или графический пользовательский интерфейс, с помощью которого можно: добавить студента, удалить
студента по уникальному номеру, вывести список студентов.

#### Rest API

* GET /list - no params
* POST /add - {firstName, secondName, thirdName, birthDate, group}
* POST /delete - {id}

#### Стек

* Spring Boot
* Spring Data
* H2
* Liquibase
* Lombok
* Spring Boot Test
* JUnit 5
* Mockito

