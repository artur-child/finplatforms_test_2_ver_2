package ru.art.api;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import ru.art.api.json.ErrorResponse;
import ru.art.api.json.StudentAddRequest;
import ru.art.api.json.StudentDeleteRequest;
import ru.art.api.json.StudentResponse;
import ru.art.service.Converter;
import ru.art.service.StudentDTO;
import ru.art.service.StudentService;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
public class StudentRestController {
    private final StudentService studentService;
    private final Converter converter;

    @GetMapping("/list")
    public ResponseEntity<List<StudentResponse>> getAllStudents() {
        List<StudentResponse> collect = studentService.listOfStudents().stream()
                .map(e -> converter.convert(e, StudentResponse.class))
                .collect(Collectors.toList());
        return ResponseEntity.ok(collect);
    }

    @PostMapping("/delete")
    public ResponseEntity<ErrorResponse> deleteStudent(@RequestBody @Valid StudentDeleteRequest studentDeleteRequest) {
        StudentDTO studentDTO = converter.convert(studentDeleteRequest, StudentDTO.class);
        if (!studentService.deleteStudent(studentDTO)) {
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new ErrorResponse("Delete fail"));
        }
        return ResponseEntity.ok().build();
    }

    @PostMapping("/add")
    public ResponseEntity<StudentResponse> addStudent(@RequestBody @Valid StudentAddRequest studentAddRequest) {
        StudentDTO studentDTO = converter.convert(studentAddRequest, StudentDTO.class);
        StudentDTO addStudent = studentService.addStudent(studentDTO);
        if (addStudent == null) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Add fail");
        }
        return ResponseEntity.ok().body(converter.convert(addStudent, StudentResponse.class));
    }
}
