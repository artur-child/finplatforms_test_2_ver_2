package ru.art.service;

import org.springframework.stereotype.Service;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class Converter {

    public <T> T convert(Object source, Class<T> targetClass) {
        T target = null;
        if (source != null) {
            try {
                Constructor<T> constructor = targetClass.getConstructor();
                target = constructor.newInstance();
            } catch (NoSuchMethodException | InvocationTargetException | InstantiationException |
                     IllegalAccessException e) {
                throw new RuntimeException("Can't construct new instance", e);
            }
            convertHelper(source, target);
        }
        return target;
    }

    private void convertHelper(Object source, Object target) {
        if (target != null) {
            Field[] targetDeclaredFields = target.getClass().getDeclaredFields();
            Set<String> sourceFieldsStrings = Arrays.stream(source.getClass().getDeclaredFields())
                    .map(Field::getName)
                    .collect(Collectors.toSet());

            for (Field targetDeclaredField : targetDeclaredFields) {
                if (sourceFieldsStrings.contains(targetDeclaredField.getName())) {
                    String getterMethodName = getGetterMethod(targetDeclaredField.getName());
                    String setterMethodName = getSetterMethod(targetDeclaredField.getName());
                    try {
                        Object sourceData = source.getClass().getMethod(getterMethodName).invoke(source);
                        target.getClass().getMethod(setterMethodName, targetDeclaredField.getType()).invoke(target, sourceData);
                    } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                        throw new RuntimeException(exceptionString(source, target), e);
                    }
                }
            }
        }
    }

    private String getGetterMethod(String s) {
        return "get" + s.substring(0, 1).toUpperCase() + s.substring(1);
    }

    private String getSetterMethod(String s) {
        return "set" + s.substring(0, 1).toUpperCase() + s.substring(1);
    }

    private String exceptionString(Object source, Object target) {
        return "Problem in conversion from source '" + source.getClass().getSimpleName() + "' to target '" + target.getClass().getSimpleName() + "'";
    }

}
