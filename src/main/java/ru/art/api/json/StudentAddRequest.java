package ru.art.api.json;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import java.time.LocalDate;

@Data
@Accessors(chain = true)
public class StudentAddRequest {
    @NotEmpty
    private String firstName;

    @NotEmpty
    private String secondName;

    private String thirdName;
    private LocalDate birthDate;

    @NotEmpty
    private String group;
}
