package ru.art.api;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import ru.art.api.json.StudentAddRequest;
import ru.art.api.json.StudentDeleteRequest;
import ru.art.api.json.StudentResponse;
import ru.art.service.StudentDTO;
import ru.art.service.StudentService;

import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
@SpringBootTest
class StudentRestControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper om;

    @SpyBean
    StudentService studentService;

    @DynamicPropertySource
    static void dynamicProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.liquibase.change-log", () -> "classpath:/dbchangelog_student_test.xml");
    }

    @BeforeEach
    void setUp() {
    }

    @Test
    void getAllStudents() throws Exception {
        StudentResponse student1 = new StudentResponse()
                .setId(1L)
                .setFirstName("first_name_1")
                .setSecondName("second_name_1")
                .setThirdName("third_name_1")
                .setBirthDate(LocalDate.of(2000, 2, 1))
                .setGroup("gruppa_1");
        StudentResponse student2 = new StudentResponse()
                .setId(2L)
                .setFirstName("first_name_2")
                .setSecondName("second_name_2")
                .setThirdName("third_name_2")
                .setBirthDate(LocalDate.of(2000, 2, 2))
                .setGroup("gruppa_2");

        MvcResult mvcResult = mockMvc.perform(get("/list"))
                .andExpect(status().isOk())
                .andReturn();

        MockHttpServletResponse response = mvcResult.getResponse();
        List<StudentResponse> studentResponses = om.readValue(
                response.getContentAsString(),
                new TypeReference<List<StudentResponse>>() {
                });

        assertEquals(studentResponses.get(0), student1);
        assertEquals(studentResponses.get(1), student2);
    }

    @Test
    void deleteStudent_success() throws Exception {
        StudentDeleteRequest studentDeleteRequest = new StudentDeleteRequest().setId(3L);
        mockMvc.perform(post("/delete")
                        .content(om.writeValueAsString(studentDeleteRequest)).contentType(APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void deleteStudent_failDueToNoSuchStudentId() throws Exception {
        StudentDeleteRequest studentDeleteRequest = new StudentDeleteRequest().setId(30L);
        mockMvc.perform(post("/delete")
                        .content(om.writeValueAsString(studentDeleteRequest)).contentType(APPLICATION_JSON))
                .andExpect(status().is5xxServerError());
    }

    @Test
    void deleteStudent_badRequest() throws Exception {
        StudentDeleteRequest studentDeleteRequest = new StudentDeleteRequest();
        mockMvc.perform(post("/delete")
                        .content(om.writeValueAsString(studentDeleteRequest)).contentType(APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    void addStudent_success() throws Exception {
        StudentAddRequest studentAddRequest = new StudentAddRequest()
                .setFirstName("first_name_4")
                .setSecondName("second_name_4")
                .setThirdName("third_name_4")
                .setBirthDate(LocalDate.of(2000, 2, 4))
                .setGroup("gruppa_4");

        StudentResponse studentResponse = new StudentResponse()
                .setId(4L)
                .setFirstName("first_name_4")
                .setSecondName("second_name_4")
                .setThirdName("third_name_4")
                .setBirthDate(LocalDate.of(2000, 2, 4))
                .setGroup("gruppa_4");

        mockMvc.perform(post("/add")
                        .content(om.writeValueAsString(studentAddRequest)).contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(om.writeValueAsString(studentResponse)));
    }

    @Test
    void addStudent_badRequestFirstName() throws Exception {
        StudentAddRequest studentAddRequest = new StudentAddRequest()
                .setSecondName("second_name_4")
                .setThirdName("third_name_4")
                .setBirthDate(LocalDate.of(2000, 2, 4))
                .setGroup("gruppa_4");

        mockMvc.perform(post("/add")
                        .content(om.writeValueAsString(studentAddRequest)).contentType(APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    void addStudent_badRequestSecondName() throws Exception {
        StudentAddRequest studentAddRequest = new StudentAddRequest()
                .setFirstName("first_name_4")
                .setThirdName("third_name_4")
                .setBirthDate(LocalDate.of(2000, 2, 4))
                .setGroup("gruppa_4");

        mockMvc.perform(post("/add")
                        .content(om.writeValueAsString(studentAddRequest)).contentType(APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    void addStudent_badRequestGroupIsEmpty() throws Exception {
        StudentAddRequest studentAddRequest = new StudentAddRequest()
                .setFirstName("first_name_4")
                .setSecondName("second_name_4")
                .setThirdName("third_name_4")
                .setBirthDate(LocalDate.of(2000, 2, 4))
                .setGroup("");

        mockMvc.perform(post("/add")
                        .content(om.writeValueAsString(studentAddRequest)).contentType(APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    void addStudent_failNullFromService() throws Exception {

        StudentAddRequest studentAddRequest = new StudentAddRequest()
                .setFirstName("first_name_4")
                .setSecondName("second_name_4")
                .setThirdName("third_name_4")
                .setBirthDate(LocalDate.of(2000, 2, 4))
                .setGroup("gruppa_4");

        StudentDTO studentDTO = new StudentDTO()
                .setFirstName("first_name_4")
                .setSecondName("second_name_4")
                .setThirdName("third_name_4")
                .setBirthDate(LocalDate.of(2000, 2, 4))
                .setGroup("gruppa_4");

        when(studentService.addStudent(studentDTO)).thenReturn(null);
        mockMvc.perform(post("/add")
                        .content(om.writeValueAsString(studentAddRequest)).contentType(APPLICATION_JSON))
                .andExpect(status().is5xxServerError());
    }
}